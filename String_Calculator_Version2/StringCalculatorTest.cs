﻿using System;
using NUnit.Framework;
namespace String_Calculator_Version2
{
    [TestFixture]
    public class StringCalculatorTest
    {
        [Test]
        public void GivenEmptyString_WhenAdding_ThenReturnZero()
        {
            //Arrange
            var calculator = new StringCalculator();

            //Act
            var result = calculator.Add("");
            //Assert
            Assert.AreEqual(0, result);
        }

        [Test]
        public void GivenTwoNumbers_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var calculator = new StringCalculator();

            //Act
            var result = calculator.Add("1,2");

            //Assert
            Assert.AreEqual(3, result);
        }

        [Test]
        public void GivenAnyAmountOfNumbers_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var calculator = new StringCalculator();

            //Act
            var result = calculator.Add("10,1,1,1,2,5");

            //Assert
            Assert.AreEqual(20, result);
        }

        [Test]
        public void GivenNewLinesBetweenNumbers_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var calculator = new StringCalculator();

            //Act
            var result = calculator.Add("1\n2,3");

            //Assert
            Assert.AreEqual(6, result);
        }

        [Test]
        public void GivenDifferentDelimiters_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var calculator = new StringCalculator();

            //Act
            var result = calculator.Add("//;\n1;2");

            //Assert
            Assert.AreEqual(3, result);
        }
        [Test]
        public void Given_One_Number_When_Adding_Then_Return_GivenNumber()
        {
            //Arrange
            var calculator = new StringCalculator();

            //Act
            var result = calculator.Add("1");

            //Assert
            Assert.AreEqual(1, result);
        }

        [Test]
        public void GivenNegativeNumbers_WhenAdding_ThenThrowException()
        {
            //Arrange
            var calculator = new StringCalculator();

            //Act
            var exception = Assert.Throws<Exception>(() => calculator.Add("//;\n2;-3;-4"));

            //Assert
            Assert.AreEqual("Negatives not allowed. -3 -4", exception.Message);
        }

        [Test]
        public void GivenIgnoreNumberGreaterThanThousand_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var calculator = new StringCalculator();

            //Act
            var result = calculator.Add("//;\n2;1001;1");

            //Assert
            Assert.AreEqual(3, result);
        }

        [Test]
        public void GivenAnyNumberOfDelimiters_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var calculator = new StringCalculator();

            //Act
            var result = calculator.Add("//***\n1***2***3");

            //Assert
            Assert.AreEqual(6, result);

        }

        [Test]
        public void GivenMultipleDelimimters_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var calculator = new StringCalculator();

            //Act
            var result = calculator.Add("//[*][%]\n1*2%3");

            //Assert
            Assert.AreEqual(6, result);
        }

        [Test]
        public void GivenInvalidDelimiter_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var calculator = new StringCalculator();

            //Act
            var exception = Assert.Throws<Exception>(() => calculator.Add("//[*][%]\n1*2$3"));

            //Assert
            Assert.AreEqual("Invalid Delimiter !. $", exception.Message);
        }
    }
}
