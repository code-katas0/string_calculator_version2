﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace String_Calculator_Version2
{
    public class StringCalculator
    {
        public int Add(string numbers)
        {
            List<string> delimiterList = new List<string>(new string[] { ",", "\n", ";" });

            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            if (numbers.Contains("//"))
            {
                //umbers = string.Concat(numbers.);
                numbers = string.Concat(numbers.Split('/'));
                delimiterList = GetDelimiters(numbers);
                numbers = string.Concat(numbers.Split('/', '\n', '[', ']'));
            }

            string[] numberList = numbers.Split(delimiterList.ToArray(), StringSplitOptions.RemoveEmptyEntries);
            ValidateDelimiters(numberList);
            ValidateNumbers(numberList);
            int total = GetTotal(numberList);

            return total;
        }
        private List<string> GetDelimiters(string numbers)
        {
            List<string> delimiterList = new List<string>();
            string delimiters = numbers.Substring(0, numbers.IndexOf("\n"));
            if (numbers.Contains("]"))
            {
                //delimiters = string.Concat();
                foreach (var delimiter in delimiters.Split('[', ']'))
                {
                    if (!string.IsNullOrEmpty(delimiters))
                    {
                        delimiterList.Add(delimiter.ToString());
                    }
                }
            }
            else
            {

                delimiterList.Add(delimiters);
                //}
            }

            return delimiterList;
        }

        private void ValidateDelimiters(string[] numberList)
        {
            char[] eachnumberList = string.Concat(numberList).ToCharArray();
            string invalidDelimters = string.Empty;

            foreach (var number in eachnumberList)
            {
                if (!int.TryParse(number.ToString(), out int realnumber) && !number.ToString().Contains("-"))
                {
                    invalidDelimters = string.Join(" ", invalidDelimters, number);
                }
            }

            if (!string.IsNullOrEmpty(invalidDelimters))
            {
                throw new Exception("Invalid Delimiter !." + invalidDelimters);
            }
        }

        private void ValidateNumbers(string[] numberList)
        {
            string negativeNumbers = string.Empty;
            foreach (var number in numberList)
            {
                int realNumber = int.Parse(number);

                if (realNumber < 0)
                {
                    negativeNumbers = string.Join(" ", negativeNumbers, number);
                }
            }

            if (!string.IsNullOrEmpty(negativeNumbers))
            {
                throw new Exception("Negatives not allowed." + negativeNumbers);
            }

        }

        private int GetTotal(string[] numberList)
        {
            int sum = 0;

            foreach (var number in numberList)
            {
                int realNumber = int.Parse(number);
                if (realNumber <= 1000)
                {
                    sum += realNumber;
                }
            }

            return sum;
        }
    }
}
